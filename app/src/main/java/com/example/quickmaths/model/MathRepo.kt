package com.example.quickmaths.model

import com.example.quickmaths.model.remote.MathService
import javax.inject.Inject

/**
 * Math repo.
 *
 * @property service
 * @constructor Create empty Math repo
 */
class MathRepo @Inject constructor(private val service: MathService) {
    /**
     * Evaluates mathematical expression passed in.
     *
     * @param expr mathematical expression to be evaluated
     * @return evaluation from mathematical expression
     */
    suspend fun evaluateExpression(expr: String): String {
        return service.evaluateExpression(expr)
    }
}
