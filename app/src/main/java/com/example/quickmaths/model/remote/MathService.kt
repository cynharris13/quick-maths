package com.example.quickmaths.model.remote

import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Math service.
 *
 * @constructor Create empty Math service
 */
interface MathService {
    @GET(VERSION)
    suspend fun evaluateExpression(@Query("expr", encoded = true) expr: String): String

    companion object {
        private const val VERSION = "v4/"
    }
}
