package com.example.quickmaths.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.quickmaths.ui.theme.QuickMathsTheme
import com.example.quickmaths.viewmodel.MathViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Result fragment.
 *
 * @constructor Create empty Result fragment
 */
@AndroidEntryPoint
class ResultFragment : Fragment() {
    private val args by navArgs<ResultFragmentArgs>()
    private val mathViewModel by viewModels<MathViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val firstInput = args.firstInput
        val operator = args.operator
        val secondInput = args.secondInput
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by mathViewModel.result.collectAsState()
                mathViewModel.evaluateExpr(
                    firstInput = firstInput,
                    op = operator,
                    secondInput = secondInput
                )
                QuickMathsTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Text(text = "${state.expression} = ${state.result}")
                    }
                }
            }
        }
    }
}
