package com.example.quickmaths.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.input.KeyboardType
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.quickmaths.ui.theme.QuickMathsTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * Second input fragment.
 *
 * @constructor Create empty Second input fragment
 */
@AndroidEntryPoint
class SecondInputFragment : Fragment() {
    private val args by navArgs<SecondInputFragmentArgs>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val firstInput = args.firstInput
        val operator = args.operator
        return ComposeView(requireActivity()).apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                QuickMathsTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        var text by remember { mutableStateOf("0") }
                        Column() {
                            Row() {
                                Column() {
                                    TextField(
                                        value = text,
                                        onValueChange = { newText ->
                                            text = newText.filter { c -> c.isDigit() }
                                        },
                                        label = { Text("Field 2") },
                                        keyboardOptions = KeyboardOptions(
                                            keyboardType = KeyboardType.Number
                                        )
                                    )
                                }
                            }
                            Row() {
                                Button(
                                    onClick = {
                                        val direction = SecondInputFragmentDirections
                                            .actionSecondInputFragmentToResultFragment(
                                                firstInput = firstInput,
                                                operator = operator,
                                                secondInput = text
                                            )
                                        findNavController().navigate(direction)
                                    }
                                ) { Text(text = "Next") }
                            }
                        }
                    }
                }
            }
        }
    }
}
