package com.example.quickmaths.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.quickmaths.ui.theme.QuickMathsTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * Operator selector fragment.
 *
 * @constructor Create empty Operator selector fragment
 */
@AndroidEntryPoint
class OperatorSelectorFragment : Fragment() {
    private val args by navArgs<OperatorSelectorFragmentArgs>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val firstInput = args.firstInput
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                QuickMathsTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Button(
                                onClick = {
                                    val direction = getDirection(firstInput, PLUS)
                                    findNavController().navigate(direction)
                                }
                            ) { Text(text = PLUS) }
                            Button(
                                onClick = {
                                    val direction = getDirection(firstInput, MINUS)
                                    findNavController().navigate(direction)
                                }
                            ) { Text(text = MINUS) }
                            Button(
                                onClick = {
                                    val direction = getDirection(firstInput, TIMES)
                                    findNavController().navigate(direction)
                                }
                            ) { Text(text = TIMES) }
                            Button(
                                onClick = {
                                    val direction = getDirection(firstInput, DIVIDE)
                                    findNavController().navigate(direction)
                                }
                            ) { Text(text = DIVIDE) }
                        }
                    }
                }
            }
        }
    }
    companion object {
        const val PLUS = "+"
        const val MINUS = "-"
        const val TIMES = "*"
        const val DIVIDE = "/"
    }
}

/**
 * Get direction.
 *
 * @param firstInput
 * @param op
 * @return
 */
fun getDirection(firstInput: String, op: String): NavDirections {
    return OperatorSelectorFragmentDirections
        .actionOperatorSelectorFragmentToSecondInputFragment(
            firstInput = firstInput,
            operator = op
        )
}
