package com.example.quickmaths.view

import androidx.fragment.app.FragmentActivity
import com.example.quickmaths.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : FragmentActivity(R.layout.activity_main)
