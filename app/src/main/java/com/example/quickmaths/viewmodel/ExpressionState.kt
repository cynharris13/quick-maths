package com.example.quickmaths.viewmodel

/**
 * Expression state.
 *
 * @property result
 * @property isLoading
 * @property expression
 * @constructor Create empty Expression state
 */
data class ExpressionState(
    val result: String = "",
    val isLoading: Boolean = false,
    val expression: String = ""
)
