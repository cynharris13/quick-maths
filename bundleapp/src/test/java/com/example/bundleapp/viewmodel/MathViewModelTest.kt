package com.example.bundleapp.viewmodel

import com.example.bundleapp.CoroutinesTestExtension
import com.example.bundleapp.model.MathRepo
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class MathViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val repo = mockk<MathRepo>()
    val mathViewModel: MathViewModel = MathViewModel(repo)

    @Test
    @DisplayName("Tests addition")
    fun testViewModelAddition() = runTest(extension.testDispatcher) {
        // given
        val expected = "4"
        coEvery { repo.evaluateExpression("2%2B2") } coAnswers { expected }
        // when
        mathViewModel.evaluateExpr("2", "2", "+")
        // then
        Assertions.assertFalse(mathViewModel.result.value.isLoading)
        Assertions.assertEquals(expected, mathViewModel.result.value.result)
    }

    @Test
    @DisplayName("Tests subtraction")
    fun testViewModelSubtraction() = runTest(extension.testDispatcher) {
        // given
        val expected = "4"
        coEvery { repo.evaluateExpression("6-2") } coAnswers { expected }
        // when
        mathViewModel.evaluateExpr("6", "2", "-")
        // then
        Assertions.assertFalse(mathViewModel.result.value.isLoading)
        Assertions.assertEquals(expected, mathViewModel.result.value.result)
    }

    @Test
    @DisplayName("Tests multiplication")
    fun testViewModelProduct() = runTest(extension.testDispatcher) {
        // given
        val expected = "4"
        coEvery { repo.evaluateExpression("2*2") } coAnswers { expected }
        // when
        mathViewModel.evaluateExpr("2", "2", "*")
        // then
        Assertions.assertFalse(mathViewModel.result.value.isLoading)
        Assertions.assertEquals(expected, mathViewModel.result.value.result)
    }

    @Test
    @DisplayName("Tests division")
    fun testViewModelQuotient() = runTest(extension.testDispatcher) {
        // given
        val expected = "4"
        coEvery { repo.evaluateExpression("8%2F2") } coAnswers { expected }
        // when
        mathViewModel.evaluateExpr("8", "2", "/")
        // then
        Assertions.assertFalse(mathViewModel.result.value.isLoading)
        Assertions.assertEquals(expected, mathViewModel.result.value.result)
    }
}
