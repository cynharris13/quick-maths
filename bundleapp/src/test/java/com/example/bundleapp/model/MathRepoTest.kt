package com.example.bundleapp.model

import com.example.bundleapp.CoroutinesTestExtension
import com.example.bundleapp.model.remote.MathService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class MathRepoTest {
    @RegisterExtension
    private val extension = CoroutinesTestExtension()
    private val mockService = mockk<MathService>()
    private val repo = MathRepo(mockService)

    @Test
    @DisplayName("Testing that we can evaluate addition")
    fun testEvaluateAddition() = runTest(extension.testDispatcher) {
        // Given
        val expr = "2%2B2"
        val result = "4"
        coEvery { mockService.evaluateExpression(expr) } coAnswers { result }

        // When
        val testResult = repo.evaluateExpression(expr)

        // Then
        Assertions.assertEquals(result, testResult)
    }

    @Test
    @DisplayName("Testing that we can evaluate subtraction")
    fun testEvaluateSubtraction() = runTest(extension.testDispatcher) {
        // Given
        val expr = "6-2"
        val result = "4"
        coEvery { mockService.evaluateExpression(expr) } coAnswers { result }

        // When
        val testResult = repo.evaluateExpression(expr)

        // Then
        Assertions.assertEquals(result, testResult)
    }

    @Test
    @DisplayName("Testing that we can evaluate multiplication")
    fun testEvaluateProduct() = runTest(extension.testDispatcher) {
        // Given
        val expr = "2*2"
        val result = "4"
        coEvery { mockService.evaluateExpression(expr) } coAnswers { result }

        // When
        val testResult = repo.evaluateExpression(expr)

        // Then
        Assertions.assertEquals(result, testResult)
    }

    @Test
    @DisplayName("Testing that we can evaluate division")
    fun testEvaluateQuotient() = runTest(extension.testDispatcher) {
        // Given
        val expr = "8%2F2"
        val result = "4"
        coEvery { mockService.evaluateExpression(expr) } coAnswers { result }

        // When
        val testResult = repo.evaluateExpression(expr)

        // Then
        Assertions.assertEquals(result, testResult)
    }
}
