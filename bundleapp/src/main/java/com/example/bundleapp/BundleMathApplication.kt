package com.example.bundleapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Bundle math application.
 *
 * @constructor Create empty Bundle math application
 */
@HiltAndroidApp
class BundleMathApplication : Application()
