package com.example.bundleapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.bundleapp.R
import com.example.bundleapp.ui.theme.QuickMathsTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * Operator selector fragment.
 *
 * @constructor Create empty Operator selector fragment
 */
@AndroidEntryPoint
class OperatorSelectorFragment : Fragment() {
    private val firstInput by lazy { arguments?.getString(FIRST) ?: "" }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                QuickMathsTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Button(
                                onClick = {
                                    val args = bundleOf(FIRST to firstInput, OP to PLUS)
                                    findNavController().navigate(
                                        R.id.action_operatorSelectorFragment_to_secondInputFragment,
                                        args
                                    )
                                }
                            ) { Text(text = PLUS) }
                            Button(
                                onClick = {
                                    val args = bundleOf(FIRST to firstInput, OP to MINUS)
                                    findNavController().navigate(
                                        R.id.action_operatorSelectorFragment_to_secondInputFragment,
                                        args
                                    )
                                }
                            ) { Text(text = MINUS) }
                            Button(
                                onClick = {
                                    val args = bundleOf(FIRST to firstInput, OP to TIMES)
                                    findNavController().navigate(
                                        R.id.action_operatorSelectorFragment_to_secondInputFragment,
                                        args
                                    )
                                }
                            ) { Text(text = TIMES) }
                            Button(
                                onClick = {
                                    val args = bundleOf(FIRST to firstInput, OP to DIVIDE)
                                    findNavController().navigate(
                                        R.id.action_operatorSelectorFragment_to_secondInputFragment,
                                        args
                                    )
                                }
                            ) { Text(text = DIVIDE) }
                        }
                    }
                }
            }
        }
    }
    companion object {
        const val PLUS = "+"
        const val MINUS = "-"
        const val TIMES = "*"
        const val DIVIDE = "/"
        const val FIRST = "firstInput"
        const val OP = "operator"
    }
}
