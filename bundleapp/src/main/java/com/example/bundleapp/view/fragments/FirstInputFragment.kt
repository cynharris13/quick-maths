package com.example.bundleapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.input.KeyboardType
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.bundleapp.R
import com.example.bundleapp.ui.theme.QuickMathsTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * First input fragment.
 *
 * @constructor Create empty First input fragment
 */
@AndroidEntryPoint
class FirstInputFragment : Fragment() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                QuickMathsTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        var text by remember { mutableStateOf("0") }
                        Column() {
                            Row() {
                                Column() {
                                    TextField(
                                        value = text,
                                        onValueChange = { newText ->
                                            text = newText.filter { c -> c.isDigit() }
                                        },
                                        label = { Text("Field 1") },
                                        keyboardOptions = KeyboardOptions(
                                            keyboardType = KeyboardType.Number
                                        )
                                    )
                                }
                            }
                            Row() {
                                Button(
                                    onClick = {
                                        val args = bundleOf("firstInput" to text)
                                        findNavController().navigate(
                                            R.id.action_firstInputFragment_to_operatorSelectorFragment,
                                            args
                                        )
                                    }
                                ) { Text(text = "Next") }
                            }
                        }
                    }
                }
            }
        }
    }
}
