package com.example.bundleapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bundleapp.model.MathRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Math view model.
 *
 * @property repo
 * @constructor Create empty Math view model
 */
@HiltViewModel
class MathViewModel @Inject constructor(val repo: MathRepo) : ViewModel() {
    private val _result: MutableStateFlow<ExpressionState> = MutableStateFlow(ExpressionState())
    val result: StateFlow<ExpressionState> get() = _result

    /**
     * Set expression text.
     *
     * @param expr
     */
    private fun setExpression(expr: String) {
        _result.update { it.copy(expression = expr) }
    }

    /**
     * Evaluate expression.
     *
     * @param firstInput
     * @param secondInput
     * @param op
     */
    fun evaluateExpr(firstInput: String, secondInput: String, op: String) = viewModelScope.launch {
        setExpression("$firstInput $op $secondInput")
        val expression = when (op) {
            "+" -> "$firstInput%2B$secondInput"
            "/" -> "$firstInput%2F$secondInput"
            else -> firstInput + op + secondInput
        }
        _result.update { it.copy(isLoading = true) }
        val response = repo.evaluateExpression(expression)
        _result.update { it.copy(isLoading = false, result = response) }
    }
}
